// variables that hold the values for the current operation
let first = "";
let op = "";
let second = "";

// buttons
const clearButton = document.getElementById('clear');
const backButton = document.getElementById('back');
const divideButton = document.getElementById('divide');
const sevenButton = document.getElementById('seven');
const eightButton = document.getElementById('eight');
const nineButton = document.getElementById('nine');
const timesButton = document.getElementById('times');
const fourButton = document.getElementById('four');
const fiveButton = document.getElementById('five');
const sixButton = document.getElementById('six');
const minusButton = document.getElementById('minus');
const oneButton = document.getElementById('one');
const twoButton = document.getElementById('two');
const threeButton = document.getElementById('three');
const plusButton = document.getElementById('plus');
const zeroButton = document.getElementById('zero');
const equalButton = document.getElementById('equal');

// screen element
const screen = document.getElementById('screen')

// clears calculator to a new state
function clear() {
    first = "";
    op = "";
    second = "";
    updateScreen(true);
}

// handles logic for clicking the backbuttton
function del() {
    if (second !== "") {
        second = second.slice(0, second.length -1);
    } else if (op !== "") {
        op = "";
    } else if (first !== "") {
        first = first.slice(0, second.length -1);
    }
    updateScreen();
}

// updates the calculator screen
function updateScreen(init = false) {
    console.log(first)
    if (init) {
        screen.innerHTML = `0`;
    } else {
        screen.innerHTML = `${first} ${op} ${second}`;
    }
}

// handles logic when a number is clicked
function appendDigit() {
    if (op === "") {
        if (first === "NaN") {
            console.log("HERE")
            first = this.innerHTML
        } else {
            first += this.innerHTML;
        }
    } else {
        second += this.innerHTML;
    }
    updateScreen();
}

// handles choosing a operator for the calculation
function addOp() {
    op = this.innerHTML;
    updateScreen()
}

// handles the calculation when equals is pressed
function eval() {
    let result = 0;
    if (op === "+") {
        result = parseInt(first) + parseFloat(second);
    } else if (op === "-") {
        result = parseInt(first) - parseFloat(second);
    } else if (op === "×") {
        result = parseInt(first) * parseFloat(second);
    } else if (op === "÷") {
        if (parseInt(second) === 0) {
            alert("Can't divide by zero")
            clear()
            return
        }
        result = ((parseInt(first) / parseInt(second)) * 1000).toFixed(0) / 1000;
    } else {
        return
    }

    clear()
    first = "" + result
    updateScreen()
}

// clear button click listener
clearButton.addEventListener('click', clear);
backButton.addEventListener('click', del)

// digit on click listeners
zeroButton.addEventListener('click', appendDigit);
oneButton.addEventListener('click', appendDigit);
twoButton.addEventListener('click', appendDigit);
threeButton.addEventListener('click', appendDigit);
fourButton.addEventListener('click', appendDigit);
fiveButton.addEventListener('click', appendDigit);
sixButton.addEventListener('click', appendDigit);
sevenButton.addEventListener('click', appendDigit);
eightButton.addEventListener('click', appendDigit);
nineButton.addEventListener('click', appendDigit);

// operation on click listeners
plusButton.addEventListener('click', addOp);
minusButton.addEventListener('click', addOp);
timesButton.addEventListener('click', addOp);
divideButton.addEventListener('click', addOp);

equalButton.addEventListener('click', eval);

// start with a clear calculator
clear();


